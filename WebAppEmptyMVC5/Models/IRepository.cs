﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public interface IRepository
    {
        IQueryable<Document> Docs { get; }
        IQueryable<DocumentHistoryRecord> DocHistory { get; }

        void SaveDocument(Document item);
        void DeleteDocument(long id);
        void SaveDocumentData(Document document, byte[] body);
    }
}