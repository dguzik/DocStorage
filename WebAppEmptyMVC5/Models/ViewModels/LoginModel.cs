﻿using System.ComponentModel.DataAnnotations;

namespace WebAppEmptyMVC5.Models.ViewModels
{
    public class LoginModel
    {
        public LoginModel()
        {

        }

        public LoginModel(string returnUrl)
        {
            ReturnUrl = returnUrl;
        }

        [UIHint("HiddenInput")]
        public string ReturnUrl { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [MinLength(6)]
        [UIHint("Password")]
        public string Password { get; set; }
    }
}