﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace WebAppEmptyMVC5.Models.ViewModels
{
    public class OpenDocumentModel
    {
        public OpenDocumentModel()
        {

        }

        public long Id { get; set; }
        public DocumentFormat Format { get; set; }

        public DevExpress.XtraRichEdit.DocumentFormat GetDevExpressTextFormat()
        {
            switch (Format)
            {
                case DocumentFormat.Doc:
                    return DevExpress.XtraRichEdit.DocumentFormat.Doc;
                case DocumentFormat.OpenDocument:
                    return DevExpress.XtraRichEdit.DocumentFormat.OpenDocument;
                case DocumentFormat.Rtf:
                    return DevExpress.XtraRichEdit.DocumentFormat.Rtf;
                case DocumentFormat.Html:
                    return DevExpress.XtraRichEdit.DocumentFormat.Html;
                default:
                    throw new Exception("Unknown format: " + Format);
            }
        }

        public DevExpress.Spreadsheet.DocumentFormat GetDevExpressSpreadsheetFormat()
        {
            switch (Format)
            {
                case DocumentFormat.Xls:
                    return DevExpress.Spreadsheet.DocumentFormat.Xls;
                case DocumentFormat.Xlsx:
                    return DevExpress.Spreadsheet.DocumentFormat.Xlsx;
                default:
                    throw new Exception("Unknown format: " + Format);
            }
        }
    }
}