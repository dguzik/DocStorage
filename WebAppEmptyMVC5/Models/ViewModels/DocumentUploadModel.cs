﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models.ViewModels
{
    public class DocumentUploadModel
    {
        public string Comment { get; set; }
        public long Category { get; set; }
        public HttpPostedFileWrapper File { get; set; }
    }
}