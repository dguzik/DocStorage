﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppEmptyMVC5.Models.ViewModels
{
    public class UserRegistrationModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare(nameof(Password))]
        public string PasswordRepeat { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Compare(nameof(EmailRepeat))]
        public string EmailRepeat { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; internal set; }
    }
}