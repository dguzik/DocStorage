﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public class AppDbContext: DbContext
    {
        public AppDbContext()
            : base("Default")
        {

        }

        public DbSet<Document> Docs { get; set; }
        public DbSet<DocumentHistoryRecord> DocHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Document>()
                .HasMany(d => d.History)
                .WithRequired()
                .HasForeignKey(r => r.DocumentId)
                .WillCascadeOnDelete();
            modelBuilder.Entity<DocumentHistoryRecord>()
                .HasIndex(r => new { r.DocumentId, r.LatestRecord });
        }
    }
}