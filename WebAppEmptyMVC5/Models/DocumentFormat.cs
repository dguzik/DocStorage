﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public enum DocumentFormat
    {
        Doc = 0,
        OpenDocument,
        Rtf,
        Xls,
        Xlsx,
        Html
    }
}