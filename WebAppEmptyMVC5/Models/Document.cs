﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public class Document
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long DocumentId { get; set; }
        public string Author { get; set; }
        public string FileName { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public List<DocumentHistoryRecord> History { get; set; }
        public string Comment { get; internal set; }
    }
}