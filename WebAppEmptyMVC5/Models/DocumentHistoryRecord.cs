﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public class DocumentHistoryRecord
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long DocumentHistoryRecordId { get; set; }
        public long DocumentId { get; set; }
        public DateTime Created { get; set; }
        public bool LatestRecord { get; set; }
        public byte[] Body { get; set; }
    }
}