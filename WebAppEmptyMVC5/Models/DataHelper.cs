﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppEmptyMVC5.Models
{
    public static class DataHelper
    {
        public static void SaveDocument(byte[] body, long documentId)
        {
            var repository = DependencyResolver.Current.GetService<IRepository>();
            var userName = HttpContext.Current.User.Identity.Name;
            var document = repository.Docs.First(d => d.DocumentId == documentId);
            if (document.Author != userName)
                throw new UnauthorizedAccessException();

            repository.SaveDocumentData(document, body);
        }

        public static byte[] GetDocument(long documentId)
        {
            var repository = DependencyResolver.Current.GetService<IRepository>();
            return repository.DocHistory.First(r => r.DocumentId == documentId && r.LatestRecord).Body;
        }
    }
}