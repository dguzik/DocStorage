﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Models
{
    public class Repository : IRepository
    {
        private AppDbContext _Ctx;

        public Repository(AppDbContext ctx)
        {
            _Ctx = ctx;
        }

        public IQueryable<Document> Docs => _Ctx.Docs;
        public IQueryable<DocumentHistoryRecord> DocHistory => _Ctx.DocHistory;

        public static IRepository Create(AppDbContext ctx)
        {
            return new Repository(ctx);
        }

        public void DeleteDocument(long id)
        {
            var doc = _Ctx.Docs.FirstOrDefault(d => d.DocumentId == id);
            if(doc != null)
            {
                _Ctx.Docs.Remove(doc);
                _Ctx.SaveChanges();
            }
        }

        public void SaveDocument(Document item)
        {
            if (item.DocumentId == 0)
                _Ctx.Docs.Add(item);
            else
            {
                _Ctx.Docs.Attach(item);
                _Ctx.Entry(item).State = EntityState.Modified;
            }
            _Ctx.SaveChanges();
        }

        public void SaveDocumentData(Document document, byte[] body)
        {
            var lastVersion = _Ctx.DocHistory.First(r => r.DocumentId == document.DocumentId && r.LatestRecord);
            lastVersion.LatestRecord = false;
            lastVersion = new DocumentHistoryRecord
            {
                Created = DateTime.UtcNow,
                LatestRecord = true,
                Body = body
            };
            document.History.Add(lastVersion);
            document.Updated = lastVersion.Created;
            _Ctx.SaveChanges();
        }
    }
}