﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace WebAppEmptyMVC5.Identity
{
    internal class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            var cfg = AppConfig.Instance;
            using (SmtpClient mailClient = new SmtpClient(cfg.OutgoingMailSmtpHost, cfg.OutgoingMailSmtpPort))
            {
                mailClient.EnableSsl = true;
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = new NetworkCredential(cfg.OutgoingEmail, cfg.OutgoingEmailPassword);
                mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                mailClient.Timeout = 2500;
                var msg = new MailMessage(cfg.OutgoingEmail, message.Destination)
                {
                    IsBodyHtml = true,
                    BodyEncoding = Encoding.UTF8,
                    Subject = message.Subject,
                    Body = message.Body
                };
                var sendMailTask = mailClient.SendMailAsync(msg);
                if (await Task.WhenAny(sendMailTask, Task.Delay(3000)) != sendMailTask)
                    throw new TimeoutException();
            }
        }
    }
}