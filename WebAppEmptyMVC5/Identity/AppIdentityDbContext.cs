﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Identity
{
    public class AppIdentityDbContext: IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext(): 
            base("IdentityConnectionString", false)
        {

        }

        public static AppIdentityDbContext Create() =>
            new AppIdentityDbContext();
    }
}