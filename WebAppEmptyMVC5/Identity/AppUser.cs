﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace WebAppEmptyMVC5.Identity
{
    public class AppUser: IdentityUser
    {
        public static AppUser Create()
        {
            return new AppUser();
        }

        public ClaimsIdentity GenerateUserIdentity(UserManager<AppUser> userManager)
        {
            return userManager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUser> userManager)
        {
            return Task.FromResult(GenerateUserIdentity(userManager));
        }
    }
}