﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppEmptyMVC5.Helpers
{
    public static class AuthHelper
    {
        public static bool IsAuthenticated =>
            HttpContext.Current.User?.Identity?.IsAuthenticated == true;
    }
}