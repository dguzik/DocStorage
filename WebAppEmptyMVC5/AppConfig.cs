﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WebAppEmptyMVC5
{
    public class AppConfig
    {
        private AppConfig() { }

        private static AppConfig _Instance;
        public static AppConfig Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (typeof(AppConfig))
                    {
                        if(_Instance == null)
                            _Instance = new AppConfig();
                    }
                }
                return _Instance;
            }
        }

        public string OutgoingMailSmtpHost => WebConfigurationManager.AppSettings["OutgoingMailSmtpHost"];
        public string OutgoingEmail => Environment.GetEnvironmentVariable("OUTGOING_EMAIL");
        public string OutgoingEmailPassword => Environment.GetEnvironmentVariable("OUTGOING_EMAIL_PASSWORD");
        public int OutgoingMailSmtpPort => Convert.ToInt32(WebConfigurationManager.AppSettings["OutgoingMailSmtpPort"]);
    }
}       
         