﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WebAppEmptyMVC5.App_Start
{
    public class WebApiConfig
    {
        internal static void Register(HttpConfiguration cfg)
        {
            cfg.Routes.MapHttpRoute("DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });
        }
    }
}