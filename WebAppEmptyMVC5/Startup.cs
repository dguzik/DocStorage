﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using WebAppEmptyMVC5.Identity;
using WebAppEmptyMVC5.Models;

[assembly: OwinStartup(typeof(WebAppEmptyMVC5.Startup))]

namespace WebAppEmptyMVC5
{
    public class Startup
    {
        public void Configuration(IAppBuilder builder)
        {
            builder.CreatePerOwinContext(AppIdentityDbContext.Create);
            builder.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            builder.CreatePerOwinContext<AppSignInManager>(AppSignInManager.Create);

            builder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Auth/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUser>(
                        validateInterval: TimeSpan.FromMinutes(3),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            builder.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            builder.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));
            builder.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            //builder.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions { ClientId = "", ClientSecret = "" });
        }
    }
}