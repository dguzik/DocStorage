const webpack = require("webpack");
const path = require("path");

var config = {
	entry: "./app.js",
	output: {
		path: path.resolve(__dirname, "../Scripts"),
		filename: "app.js"
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: "ts-loader"   
			},
			{
				test: /\.css$/,
				use: [
					"style-loader",
					"css-loader"//,
					//"postcss-loader"
				]
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					"css-loader",
					//"postcss-loader",
					"sass-loader"
				]
			}
			
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']
		})
	]
}

module.exports = config;