namespace WebAppEmptyMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedNullabe : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Documents", "Updated", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Documents", "Updated", c => c.DateTime(nullable: false));
        }
    }
}
