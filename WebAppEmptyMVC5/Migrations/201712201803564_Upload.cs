namespace WebAppEmptyMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Upload : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "Comment");
        }
    }
}
