namespace WebAppEmptyMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class History2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DocumentHistoryRecords", new[] { "DocumentId", "LatestRecord" });
            CreateIndex("dbo.DocumentHistoryRecords", new[] { "DocumentId", "LatestRecord" });
        }
        
        public override void Down()
        {
            DropIndex("dbo.DocumentHistoryRecords", new[] { "DocumentId", "LatestRecord" });
            CreateIndex("dbo.DocumentHistoryRecords", new[] { "DocumentId", "LatestRecord" }, unique: true);
        }
    }
}
