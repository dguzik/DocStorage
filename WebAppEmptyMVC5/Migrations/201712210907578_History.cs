namespace WebAppEmptyMVC5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class History : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocumentHistoryRecords",
                c => new
                    {
                        DocumentHistoryRecordId = c.Long(nullable: false, identity: true),
                        DocumentId = c.Long(nullable: false),
                        Created = c.DateTime(nullable: false),
                        LatestRecord = c.Boolean(nullable: false),
                        Body = c.Binary(),
                    })
                .PrimaryKey(t => t.DocumentHistoryRecordId)
                .ForeignKey("dbo.Documents", t => t.DocumentId, cascadeDelete: true)
                .Index(t => new { t.DocumentId, t.LatestRecord }, unique: true);
            
            DropColumn("dbo.Documents", "Body");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "Body", c => c.Binary());
            DropForeignKey("dbo.DocumentHistoryRecords", "DocumentId", "dbo.Documents");
            DropIndex("dbo.DocumentHistoryRecords", new[] { "DocumentId", "LatestRecord" });
            DropTable("dbo.DocumentHistoryRecords");
        }
    }
}
