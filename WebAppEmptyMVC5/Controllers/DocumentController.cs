﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebAppEmptyMVC5.Models;
using WebAppEmptyMVC5.Models.ViewModels;

namespace WebAppEmptyMVC5.Controllers
{
    [Authorize]
    public class DocumentController: Controller
    {
        private IRepository _Repository;

        public DocumentController(IRepository repo)
        {
            _Repository = repo;
        }

        public ActionResult Index() => View("Index", (object)_Repository.GetType().ToString());

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", _Repository.Docs.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew(WebAppEmptyMVC5.Models.Document item)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _Repository.SaveDocument(item);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", _Repository.Docs.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate(WebAppEmptyMVC5.Models.Document item)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _Repository.SaveDocument(item);
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", _Repository.Docs.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(long id)
        {
            _Repository.DeleteDocument(id);
            return PartialView("_GridViewPartial", _Repository.Docs.ToList());
        }

        [HttpPost]
        public async Task<ActionResult> UploadDocument(DocumentUploadModel model)
        {
            if (ModelState.IsValid)
            {
                using (var br = new MemoryStream(model.File.ContentLength))
                {
                    await model.File.InputStream.CopyToAsync(br);
                    _Repository.SaveDocument(new Document
                    {
                        Author = User.Identity.Name,
                        Created = DateTime.UtcNow,
                        Comment = model.Comment,
                        FileName = model.File.FileName,
                        History = new List<DocumentHistoryRecord>
                        {
                            new DocumentHistoryRecord
                            {
                                Created = DateTime.UtcNow,
                                LatestRecord = true,
                                Body = br.ToArray()
                            }
                        }
                    });
                }
                return Json(new { result = true });
            }
            else
            {
                return Json(new { result = false, errors = "There were some errors" });
            }
        }

        public ActionResult GetTextDocumentEditor(OpenDocumentModel model)
        {
            return PartialView("TextDocumentEditPartial", model);
        }

        public ActionResult GetSpreadsheetEditor(OpenDocumentModel model)
        {
            return PartialView("_SpreadsheetPartial", model);
        }

        public ActionResult GetDocumentEditor(OpenDocumentModel model)
        {
            var fileName = from d in _Repository.Docs
                           where d.DocumentId == model.Id
                           select d.FileName;
            
            switch (Path.GetExtension(fileName.First()).ToLower())
            {
                case ".doc":
                    model.Format = DocumentFormat.Doc;
                    break;
                case ".rtf":
                    model.Format = DocumentFormat.Rtf;
                    break;
                case ".odt":
                    model.Format = DocumentFormat.OpenDocument;
                    break;
                case ".html":
                case ".htm":
                    model.Format = DocumentFormat.Html;
                    break;
                case ".xls":
                    model.Format = DocumentFormat.Xls;
                    break;
                case ".xlsx":
                    model.Format = DocumentFormat.Xlsx;
                    break;
                default:
                    return new HttpStatusCodeResult(500);
            }
            if(model.Format == DocumentFormat.Xls || model.Format == DocumentFormat.Xlsx)
            {
                return GetSpreadsheetEditor(model);
            }
            else
            {
                return GetTextDocumentEditor(model);
            }
        }
    }
}