﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebAppEmptyMVC5.Identity;
using WebAppEmptyMVC5.Models.ViewModels;

namespace WebAppEmptyMVC5.Controllers
{
    public class ManageUserController: Controller
    {
        private AppUserManager _UserManager;
        private AppConfig _Cfg;

        public ManageUserController(AppUserManager userManager, AppConfig cfg)
        {
            _UserManager = userManager;
            _Cfg = cfg;
        }

        public ActionResult Register() => View();

        [HttpPost]
        public async Task<ActionResult> Register(UserRegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser
                {
                    UserName = model.Name,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber
                };
                var validationResult = await _UserManager.UserValidator.ValidateAsync(user);
                if (validationResult.Succeeded)
                {
                    var passwordValidationResult = await _UserManager.PasswordValidator.ValidateAsync(model.Password);
                    if (passwordValidationResult.Succeeded)
                    {
                        user.PasswordHash = _UserManager.PasswordHasher.HashPassword(model.Password);
                        var creationResult = await _UserManager.CreateAsync(user);
                        if (creationResult.Succeeded)
                        {
                            HttpContext.GetOwinContext().Authentication.SignIn(user.GenerateUserIdentity(_UserManager));
                            var emailComfirmationUrl = Url.Action(nameof(ConfirmEmail), new
                            {
                                userName = user.UserName,
                                token = await _UserManager.GenerateEmailConfirmationTokenAsync(user.UserName)
                            });
                            await _UserManager.SendEmailAsync(user.UserName, "Comfirm your registration", $"<a href=\"{emailComfirmationUrl}\">Click me</a>");
                            return Redirect("/");
                        }
                        else
                            PopulateModelErrors(string.Empty, creationResult.Errors);
                    }
                    else
                    {
                        PopulateModelErrors(nameof(model.Password), passwordValidationResult.Errors);
                    }
                }
                else
                {
                    PopulateModelErrors(string.Empty, validationResult.Errors);
                }
            }
            return View(model);
        }

        public async Task<ActionResult> ConfirmEmail(string userName, string token)
        {
            var result = await _UserManager.ConfirmEmailAsync(userName, token);
            return View(result.Errors);
        }

        private void PopulateModelErrors(string property, IEnumerable<string> errors)
        {
            foreach (var error in errors)
                ModelState.AddModelError(property, error);
        }
    }
}