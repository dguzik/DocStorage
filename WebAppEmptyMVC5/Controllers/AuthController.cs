﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebAppEmptyMVC5.Identity;
using WebAppEmptyMVC5.Models.ViewModels;

namespace WebAppEmptyMVC5.Controllers
{
    public class AuthController: Controller
    {
        private AppUserManager _UserManager;

        public AuthController(AppUserManager userMgr)
        {
            _UserManager = userMgr;
        }

        public ActionResult Login(string returnUrl) => View(new LoginModel(returnUrl));

        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _UserManager.FindByNameAsync(model.Name);
                if(user != null)
                {
                    var passwordVerification = _UserManager.PasswordHasher.VerifyHashedPassword(user.PasswordHash, model.Password);
                    if(passwordVerification == PasswordVerificationResult.Success)
                    {
                        var authManager = HttpContext.GetOwinContext().Authentication;
                        var authProps = new AuthenticationProperties { IsPersistent = false };
                        authManager.SignIn(user.GenerateUserIdentity(_UserManager));
                        return Redirect(model.ReturnUrl ?? "/");
                    }
                    else
                    {
                        ModelState.AddModelError(nameof(model.Password), "Incorrect password");
                    }
                }
                else
                {
                    ModelState.AddModelError(nameof(model.Name), "User not found");
                }
            }
            return View(model);
        }

        public ActionResult Logout(string returnUrl)
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return Redirect(returnUrl ?? "/");
        }
    }
}