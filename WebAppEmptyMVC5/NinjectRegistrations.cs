﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Ninject.Activation;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppEmptyMVC5.Identity;
using WebAppEmptyMVC5.Models;

namespace WebAppEmptyMVC5
{
    public class NinjectRegistrations : NinjectModule
    {
        private const string key = "_impl+";

        public override void Load()
        {
            Bind<AppDbContext>();
            Bind<IRepository>().ToMethod(c =>
            {
                IRepository repository = GetFromHttpContext<IRepository>();
                if(repository == null)
                {
                    repository = new Repository(new AppDbContext());
                    SaveInHttpContext(repository);
                }
                return repository;
            });
            Bind<AppUserManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Get<AppUserManager>());
            Bind<AppSignInManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Get<AppSignInManager>());
            Bind<AppConfig>().ToMethod(c => AppConfig.Instance);
        }

        private AppUserManager CreateAndStoreUserManager(IContext ctx)
        {
            var mgr = GetFromHttpContext<AppUserManager>();
            if (mgr == null)
            {
                mgr = new AppUserManager(new UserStore<AppUser>(new AppIdentityDbContext()));
                SaveInHttpContext(mgr);
            }
            return mgr;
        }

        private T GetFromHttpContext<T>() where T: class
        {
            return HttpContext.Current.Items[key + typeof(T).ToString()] as T;
        }

        private void SaveInHttpContext<T>(T implementation) where T : class
        {
            HttpContext.Current.Items[key + typeof(T).ToString()] = implementation;
        }
    }
}